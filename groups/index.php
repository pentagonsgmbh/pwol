<?php
require("../assets/php/database/group.php");
?>
<!doctype html>
<html>
    <head>
        <link rel="stylesheet" href="../assets/tileosaur/css/tileosaur.min.css" />
        <link rel="stylesheet" href="../assets/images/backgrounds.css" />
        <link rel="stylesheet" href="../assets/css/custom.css" />
        <link rel="icon" href="../assets/images/icon.ico" />
        <title>PC-Gruppen &middot; pWOL</title>
    </head>
    <body class="body body-groups">
        <h1 class="page-header">
            <a href="../" class="back-icon"></a>
            PC-Gruppen
        </h1>
        
        <div class="app-container">
            <div class="container">
                <h2 class="sub-header text-red">Gruppe erstellen</h2>
                <form action="addgroup.php" method="get">
                    <input type="text" class="input-red add-pc" name="name" placeholder="Gruppenname" /><br />
                    <input type="submit" class="btn btn-red btn-add-pc" value="Erstellen" />
                </form>
            </div>
            
<?php

$group_html = "
            <div class=\"container\">
                <h2 class=\"sub-header text-red\">
                    %s
                    <a href=\"additem/?id=%s\" class=\"back-icon plus-icon\"></a>
                    <a href=\"rmgroup.php?id=%s\" class=\"back-icon trash-icon\"></a>
                </h2>
                <div class=\"tile-container\">
                    %s
                </div>
            </div>
            ";
$groups = getGroups();
foreach ($groups as $group) {
    $name = $group["name"]; $id = $group["id"];
    $pcs = "";
    foreach (getPCsInGroup($id) as $pc) {
        $ip = $pc["ip"]; $pcid = $pc["id"];
        $pcs .= "
                    <div onclick=\"allpcs.tileClick(this);\" class=\"tile tile-red\">
                        <div class=\"tile-caption\">
                            $ip
                        </div>
                        <div class=\"pc-config\">
                            <a href=\"rmitem.php?group_id=$id&pc_id=$pcid\" class=\"back-icon trash-icon trash-icon-center\"></a>
                        </div>
                    </div>";
    }
    
    printf($group_html, $name, $id, $id, $pcs);
}

?>
        </div>
        
        <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
        <script type="text/javascript" src="../assets/js/allpcs.js"></script>
    </body>
</html>