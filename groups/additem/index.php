<?php
require("../../assets/php/database/group.php");
$id = isset($_GET["id"]) ? $_GET["id"] : header("Location: ../");

?>
<!doctype html>
<html>
    <head>
        <link rel="stylesheet" href="../../assets/tileosaur/css/tileosaur.min.css" />
        <link rel="stylesheet" href="../../assets/images/backgrounds.css" />
        <link rel="stylesheet" href="../../assets/css/custom.css" />
        <link rel="icon" href="../../assets/images/icon.ico" />
        <title>PC Hinzuf&uuml;gen &middot; pWOL</title>
    </head>
    <body class="body body-groups">
        <h1 class="page-header">
            <a href="../" class="back-icon"></a>
            <?php echo getGroupName($id); ?>
        </h1>
        
        <div class="app-container">
            <div class="container">
                <h2 class="sub-header text-red">PCs ausw&auml;hlen <a href="#" onclick="groups.add(<?php echo $id; ?>);" class="btn btn-red">Hinzuf&uuml;gen</a></h2>
                <div class="tile-container">
            <?php
$pc_text = "
                    <div id=\"%s\" class=\"tile tile-red\" checkable>
                        <div class=\"check-image\"></div>
                        <div class=\"tile-caption\">
                            %s
                        </div>
                    </div>";

$pcs = getPCsNotInGroup($id);
$perLine = ceil($pcs["count"] / 3);
$i = 0;
$pcs = $pcs["result"];
foreach ($pcs as $pc) {
    if ($i > $perLine) {
        print "<br />";
        $i = 0;
    }
    
    printf($pc_text, $pc["id"], $pc["ip"]);
    
    $i += 1;
}
?>
                </div>
            </div>
        </div>
        
        
        <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
        <script type="text/javascript" src="../../assets/tileosaur/js/tileosaur.js"></script>
        <script type="text/javascript" src="../../assets/js/groups.js"></script>
    </body>
</html>