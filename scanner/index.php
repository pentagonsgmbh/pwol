<!doctype html>
<html>
    <head>
        <link rel="stylesheet" href="../assets/tileosaur/css/tileosaur.min.css" />
        <link rel="stylesheet" href="../assets/images/backgrounds.css" />
        <link rel="stylesheet" href="../assets/css/custom.css" />
        <link rel="icon" href="../assets/images/icon.ico" />
        <title>Netzwerkscanner &middot; pWOL</title>
    </head>
    <body class="body body-ethernet">
        <h1 class="page-header">
            <a href="../" class="back-icon"></a>
            Netzwerkscanner
        </h1>
        
        <div class="app-container">
            <div class="container">
                <h2 class="sub-header text-blue">Scanner-Config</h2>
                <table class="table">
                    <tbody>
                        <tr>
                            <td>nmap -sP</td>
                            <td><input type="text" id="cmd" class="input-blue" placeholder="10.5.1.*" autofocus /></td>
                        </tr>
                    </tbody>
                </table>
                <input id="btn" type="submit" class="btn btn-blue btn-start-scan" value="Starten" onclick="nmap.scan();" />
                <img src="../assets/images/gif/loading.gif" class="loading-image" />
            </div>
            <div class="container">
                <h2 class="sub-header text-blue">Resultat</h2>
                <table class="table table-zebra">
                    <thead>
                        <th>IPv4</th>
                        <th>MAC</th>
                        <th>Hostname</th>
                    </thead>
                    <tbody id="result-table">
                    </tbody>
                </table>
            </div>
        </div>
        
        <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
        <script type="text/javascript" src="../assets/js/notification.js"></script>
        <script type="text/javascript" src="../assets/js/nmap.js"></script>
    </body>
</html>