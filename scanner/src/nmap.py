import subprocess, re, sys, time

def filterMAC(text):
    m = re.findall(r"..:..:..:..:..:..", text)
    if len(m) > 0:
        m = m[0]
    return m

def filterIP(text):
    i = re.findall(r"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}", text)
    if len(i) > 0:
        i = i[0]
    return i

def filterHostname(text): 
    breaker = "Nmap scan report for "
    t = text.split(breaker)
    return t[1].split(" (")[0]


cmd = "nmap -sP " + sys.argv[1]
with open(sys.argv[2], "w") as f:
    f.write("*** SCAN STARTED ON " + time.ctime(time.time()) + " ***\n")

pipe = subprocess.Popen(cmd, shell="True", stdout=subprocess.PIPE)

temp = ""
while 1:
    line = pipe.stdout.readline()
    
    if line.startswith("Nmap"):
        mac = filterMAC(temp)
        ip = filterIP(temp)
        if len(mac) > 0 and len(ip) > 0: 
            hostname = filterHostname(temp)
            print ip, "online"
            with open(sys.argv[2], "a") as f:
                f.write("%s;%s;%s\n"%(ip, mac, hostname))
        
        temp = line
    elif len(line.strip()) != 0:
        temp += "\n" + line
    
    if not line:
        break

with open(sys.argv[2], "a") as f:
    f.write("*** FINISHED ON " + time.ctime(time.time()) + " ***")
