<?php
require("../assets/php/database/pc.php");
require("../assets/core/core.php");

if (isset($_GET["id"]) and isset($_GET["mode"])) {
    $c = getContact($_GET["id"]);
	
    switch ($_GET["mode"]) {
        case 1: 
        Core::Start($c["mac"]);
        break;
        case 2: 
        Core::Stop($c["ip"]);
        break;
        case 3: 
        Core::Restart($c["ip"]);
        break;
    }
}
header("Location: index.php");

?>