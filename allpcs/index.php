<?php
require("../assets/php/database/pc.php");
?>
<!doctype html>
<html>
    <head>
        <link rel="stylesheet" href="../assets/tileosaur/css/tileosaur.min.css" />
        <link rel="stylesheet" href="../assets/images/backgrounds.css" />
        <link rel="stylesheet" href="../assets/css/custom.css" />
        <link rel="icon" href="../assets/images/icon.ico" />
        <title>Alle PCs &middot; pWOL</title>
    </head>
    <body class="body body-awesome">
        <h1 class="page-header">
            <a href="../" class="back-icon"></a>
            Alle PCs
        </h1>
        
        <div class="app-container">
            <div class="container">
                <h2 class="sub-header text-green">PC hinzuf&uuml;gen</h2>
                <form action="addpc.php" method="get">
                    <input type="text" class="input-green add-pc" name="ip" placeholder="IPv4" /><br />
                    <input type="text" class="input-green add-pc" name="mac" placeholder="MAC" /><br />
                    <input type="text" class="input-green add-pc" name="name" placeholder="Hostname" /><br />
                    <input type="submit" class="btn btn-green btn-add-pc" value="Hinzuf&uuml;gen" />
                </form>
            </div>
            <div class="container">
                <h2 class="sub-header text-green">
                    PCs 
                    
                    <select id="mode" onchange="allpcs.selectChanged();" class="mode-select">
                        <option <?php echo (isset($_GET["mode"]) and $_GET["mode"] == "ip") ? "selected" : ""; ?> >IPv4</option>
                        <option <?php echo (isset($_GET["mode"]) and $_GET["mode"] == "mac") ? "selected" : ""; ?> >MAC</option>
                        <option <?php echo (isset($_GET["mode"]) and $_GET["mode"] == "name") ? "selected" : ""; ?> >Hostname</option>
                    </select>
                    
                    <input id="search" onkeyup="setTimeout(function () { allpcs.searchFieldText(); }, 1);" class="input-green" type="text" placeholder="Suchen..." autofocus /> 
                </h2>
                <div class="tile-container"><?php

$mode = isset($_GET["mode"]) ? $_GET["mode"] : "ip";
$list = null;
switch ($mode) {
    case "mac": 
    $list = getPCsByMAC();
    break;
    case "name": 
    $list = getPCsByHostname();
    break;
    default: 
    $list = getPCsByIP();
    $mode = "ip";
    break;
}
foreach ($list as $pc) {
    $text = $pc[$mode];
    $id = $pc["id"];
    
    #online-status via tile-color!
    
    $tile_color = "tile-green";
    $caption_color = "";
    
    echo "
                    <div onclick=\"allpcs.tileClick(this); \" class=\"tile $tile_color\">
                        
                        <div class=\"tile-caption $caption_color\">
                            $text
                        </div>
                        
                        <div class=\"pc-config $caption_color\">
                            <ul>
                                <li><a href=\"powerpc.php?id=$id&mode=2\">Herunterfahren</a></li>
                                <li><a href=\"powerpc.php?id=$id&mode=3\">Neustarten</a></li>
                                <li><a href=\"powerpc.php?id=$id&mode=1\">Starten</a></li>
                                <li><a href=\"rmpc.php?id=$id\">PC l&ouml;schen</a></li>
                            </ul>
                        </div>
                        
                    </div>
";
}

?>
                </div>
            </div>
        </div>
        
        <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
        <script type="text/javascript" src="../assets/js/allpcs.js"></script>
    </body>
</html>