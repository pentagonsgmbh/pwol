<?php
require("../assets/php/database/group.php");
?>
<!doctype html>
<html>
    <head>
        <link rel="stylesheet" href="../assets/tileosaur/css/tileosaur.min.css" />
        <link rel="stylesheet" href="../assets/images/backgrounds.css" />
        <link rel="stylesheet" href="../assets/css/custom.css" />
        <link rel="icon" href="../assets/images/icon.ico" />
        <title>Server Shell &middot; pWOL</title>
    </head>
    <body class="body body-tasks">
        <h1 class="page-header">
            <a href="../" class="back-icon"></a>
            Server Shell
        </h1>
        
        <div class="app-container">
            <div class="container">
                <div id="shell">
                    
                </div>
                <input type="text" id="command" class="monospace no-border cmd" autofocus />
                <div id="cmdsaver"></div>
            </div>
        </div>
        
        <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
        <script type="text/javascript" src="../assets/js/shell.js"></script>
    </body>
</html>