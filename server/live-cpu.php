<?php
require("../assets/php/imagecreator/index.php");
require("../assets/php/systeminfo/sysinfo.php");
$c = new CPU();

$cpu =  $c->GetUsage() / 100;

$i = new Image(300, 300);
$i->SetBackgroundcolor("#de572a");
$i->DrawGradient(0, 0, 300, 250, "#b8441d", "#de572a");
$i->PieDiagram($cpu, "#fff", "#ff926c", array(150, 150), 75);
$i->Output();
?>