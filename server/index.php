<!doctype html>
<html>
    <head>
        <link rel="stylesheet" href="../assets/tileosaur/css/tileosaur.min.css" />
        <link rel="stylesheet" href="../assets/images/backgrounds.css" />
        <link rel="stylesheet" href="../assets/css/custom.css" />
        <link rel="icon" href="../assets/images/icon.ico" />
        <title>Server-Status &middot; pWOL</title>
    </head>
    <body class="body body-tasks">
        <h1 class="page-header">
            <a href="../" class="back-icon"></a>
            Server-Status
            <img id="loader" class="loading-icon" src="../assets/images/gif/loading_orange.gif" />
        </h1>
        
        <div class="app-container" style="display: none;">
            
            <div class="container">
                <h2 class="sub-header text-orange">CPU</h2>
                <img id="cpu-usage-img" src="pie-chart.php?cpu=0.3" class="pie-chart"/>
                <table class="table">
                    <tbody>
                        <tr>
                            <td>Kerne: </td>
                            <td id="cpu-cores"></td>
                        </tr>
                        <tr>
                            <td>Auslastung: </td>
                            <td id="cpu-usage"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            
            <div class="container">
                <h2 class="sub-header text-orange">RAM</h2>
                <img id="ram-usage-img" src="pie-chart.php?cpu=0.3" class="pie-chart"/>
                <table class="table">
                    <tbody>
                        <tr>
                            <td>Verf&uuml;gbarer RAM: </td>
                            <td id="ram-avaialable"></td>
                        </tr>
                        <tr>
                            <td>Gesamter RAM: </td>
                            <td id="ram-all"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            
            <div class="container">
                <h2 class="sub-header text-orange">Betriebssystem</h2>
                <table class="table table-zebra">
                    <tbody>
                        <tr>
                            <td>Name: </td>
                            <td id="os-name"></td>
                        </tr>
                        <tr>
                            <td>Version: </td>
                            <td id="os-version"></td>
                        </tr>
                        <tr>
                            <td>Installiert: </td>
                            <td id="os-installed"></td>
                        </tr>
                        <tr>
                            <td>Hostname: </td>
                            <td id="hostname"></td>
                        </tr>
                        <tr>
                            <td>BIOS: </td>
                            <td id="bios"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            
            <div class="container">
                <h2 class="sub-header text-orange">Netzwerkadapter</h2>
                <div id="network" class="network"></div>
            </div>
        </div>
        
        <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
        <script type="text/javascript" src="../assets/js/server.js"></script>
    </body>
</html>