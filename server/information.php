<?php
require("../assets/php/systeminfo/sysinfo.php");

$c = new CPU();

$cpu = array();
$cpu["cores"] = $c->GetCores();
$cpu["usage"] = $c->GetUsage();
$cpu["image"] = "pie-chart.php?cpu=" . $cpu["usage"];


$i = new systeminfo();

$os = array();
$os["hostname"] = $i->GetHostname();
$os_info = $i->GetOS();
$os["name"] = $os_info["name"];
$os["version"] = $os_info["version"];
$os["installed"] = $os_info["install"];
$os["bios"] = $i->GetBIOS();


$r = $i->GetRAM();

$ram = array();
$ram["all"] = $r["all"];
$ram["available"] = $r["available"];
$ram["image"] = "pie-chart.php?cpu=" . ($r["available"] / $r["all"] * 100);


$na = new NetworkAdapters();


print json_encode((object)(array("os"=>$os, "cpu"=>$cpu, "nw"=>$na->adapters, "ram"=>$ram)));

?>