﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.IO;

namespace pWOL_Client
{
    public partial class Service : ServiceBase
    {
        public Service()
        {
            InitializeComponent();
        }

        private void listen() 
        {
            UdpClient listener = new UdpClient(666);
            IPEndPoint g = new IPEndPoint(IPAddress.Any, 666);
            string data;
            byte[] data_array;
            try
            {
                while (true)
                {
                    data_array = listener.Receive(ref g);
                    data = Encoding.ASCII.GetString(data_array, 0, data_array.Length);
                    if (data == "2")
                    {
                        Process.Start("shutdown.exe", "-s -f -t 0");
                    }
                    else if (data == "3")
                    {
                        Process.Start("shutdown.exe", "-r -f -t 0");
                    }
                }
            }
            catch
            { }
            listener.Close();
        }

        protected override void OnStart(string[] args)
        {
            new Thread(listen).Start();
        }

        protected override void OnStop()
        {
        }
    }
}
