<?php

class Core {
    public static function GetCoreScript() {
        return join("/", array(str_replace("\\", "/", dirname(__FILE__)), "core.exe"));
    }
    public static function Start($mac) {
        exec("\"" . Core::GetCoreScript() . "\" --start $mac");
    }
    public static function Stop($ip) {
        exec("\"" . Core::GetCoreScript() . "\" --stop $ip");
    }
    public static function Restart($ip) {
        exec("\"" . Core::GetCoreScript() . "\" --restart $ip");
    }
}

?>