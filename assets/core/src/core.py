#region imports
import argparse
import os
import re
import sqlite3
import socket
import sys
import os.path
#endregion

#region regex-filters
def filterMAC(text):
    m = re.findall(r"..:..:..:..:..:..", text)
    if len(m) > 0:
        m = m[0]
    return m

def filterIP(text):
    i = re.findall(r"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}", text)
    if len(i) > 0:
        i = i[0]
    return i
#endregion

#region group-information
def getGroupInformation(group_id): 
    output = []
    path = os.path.join(os.path.dirname(sys.argv[0]).replace("core", "php"), "database/database.sqlite").replace("\\", "/") #task is run in system32 or somewhere else -> database won't be reachable
    connection = sqlite3.connect(path)
    cursor = connection.cursor()
    cursor.execute("SELECT pcs.ip, pcs.mac FROM pglink, pcs WHERE pglink.ref_group = %s AND pglink.ref_pc = pcs.id"%group_id)
    for row in cursor.fetchall(): 
        output.append(row)
    cursor.close()
    connection.close()
    return output
    
#endregion

#region client-management
def send(ip, port, message): 
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.sendto(message, (ip, port))
    s.close()

def start(mac): 
    os.system(os.path.join(os.path.dirname(sys.argv[0]), "wol.exe") + " %s"%m)

def stop(ip): 
    send(ip, 666, "2")
    print "Stop-command sent to", ip

def restart(ip): 
    send(ip, 666, "3")
    print "Restart-command sent to", ip
#endregion

#region setting up argparser
parser = argparse.ArgumentParser(description="pWakeOnLan Core-Script - Manages Start and Stop of specified machine/group")
parser.add_argument("--start", help="starts computer using a MAC-address")
parser.add_argument("--stop", help="stops computer using an IPv4")
parser.add_argument("--restart", help="restarts computer using an IPv4")
parser.add_argument("--group-id", help="uses database-group for the desired action")
parser.add_argument("--action", help="action to use when uid is given;\n\n 1: start, 2: stop, 3: restart")

args = vars(parser.parse_args())
#endregion

#region arg-case-detection
if args["group_id"] != None and args["action"] != None:  #use class from database
    group_id = args["group_id"]; action = args["action"]
    pcs = getGroupInformation(group_id)
    print pcs
    if str(action) == "1": 
        for pc in pcs: 
            print "Sending magic-packet to", pc[0]
            start(pc[1])
    elif str(action) == "2": 
        for pc in pcs: 
            print "Sending stop-command to", pc[0]
            stop(pc[0])
    elif str(action) == "3": 
        for pc in pcs: 
            print "Sending restart-command to", pc[0]
            restart(pc[0])
    else: 
        parser.print_help()
elif args["start"] != None:  #use given mac (if correct) to wake up client
    m = filterMAC(args["start"])
    if m: 
        start(m)
    else: 
        parser.print_help()
elif args["stop"] != None:  #use given ip (if correct) to stop client
    ip = filterIP(args["stop"])
    if ip: 
        stop(ip)
    else: 
        parser.print_help()
elif args["restart"] != None:  #use given ip (if correct) to restart client
    ip = filterIP(args["restart"])
    if ip: 
        restart(ip)
    else: 
        parser.print_help()
else: #fallback -> shows help
    parser.print_help()
#endregion