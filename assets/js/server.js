/*global $:false, setTimeout:false, alert:false*/

var server = {
    updateInterval: 10000,
    loadData: function () {
        "use strict";
        $("#loader").fadeIn();
        
        $.ajax({
            url: "information.php",
            success: server.loadDataSuccess,
            error: function (xhr, status, error) {
                setTimeout(server.loadData, server.updateInterval);
            },
            dataType: "json"
        });
    },
    loadDataSuccess: function (json) {
        "use strict";
        var os = json.os, cpu = json.cpu, ram = json.ram, ipconfig = json.nw, nw = "", adapter = "", config = "";
        $("#os-name").text(os.name);
        $("#os-version").text(os.version);
        $("#os-installed").text(os.installed);
        $("#hostname").text(os.hostname);
        $("#bios").text(os.bios);
        
        $("#cpu-cores").text(cpu.cores);
        $("#cpu-usage").text(cpu.usage + "%");
        $("#cpu-usage-img").attr("src", cpu.image);
        
        $("#ram-all").text(ram.all + "MB");
        $("#ram-avaialable").text(ram.available + "MB");
        $("#ram-usage-img").attr("src", ram.image);
        
        nw = "";
        for (adapter in json.nw) {
            if (json.nw.hasOwnProperty(adapter)) {
                nw += "<b>" + adapter + "</b><table class=\"table table-zebra\"><tbody>";
                config = json.nw[adapter];
                nw += "<tr><td>Status: </td><td>" + config.state + "</td></tr>";
                nw += "<tr><td>DHCP: </td><td>" + config.dhcp + "</td></tr>";
                nw += "<tr><td>IPv4: </td><td>" + config.ip + "</td></tr>";
                nw += "<tr><td>Subnetzmaske: </td><td>" + config.subnetmask + "</td></tr>";
                
                nw += "</tbody></table><br />";
            }
        }
        $("#network").html(nw);
        
        setTimeout(server.loadData, server.updateInterval);
        
        $(".app-container").fadeIn();
        $("#loader").fadeOut();
    }
};

$(server.loadData);