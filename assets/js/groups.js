/*global $:false, window:false*/

var groups = {
    ids: "",
    add: function (id) {
        "use strict";
        $(".tile[checked=\"checked\"]").each(function (i, element) {
            groups.ids += $(this).attr("id") + ";";
        });
        
        window.location = "additems.php?group_id=" + id + "&pcs=" + groups.ids.substr(0, groups.ids.length - 1);
        return false;
    }
};