/*global $:false, alert:false, setTimeout:false*/

var shell = {
    ajax: function (url, callback) {
        "use strict";
        $.ajax({
            url: url,
            success: callback,
            error: function (xhr, status, message) {
                alert("Error: " + message);
            }
        });
    },
    
    shell: null,
    input: null,
    uid: "",
    
    initShell: function () {
        "use strict";
        
        shell.shell = $("#shell");
        shell.input = $("#command");
        
        shell.getUID();
        
        shell.input.keyup(shell.exec);
    },
    
    getUID: function () {
        "use strict";
        shell.ajax("exec.php?init", shell.getUIDSuccess);
    },
    getUIDSuccess: function (data) {
        "use strict";
        shell.uid = data;
        
        shell.getCwd();
    },
    
    getCwd: function () {
        "use strict";
        shell.ajax("shells/" + shell.uid + ".txt", shell.getCwdSuccess);
    },
    getCwdSuccess: function (data) {
        "use strict";
        $("<div class=\"monospace text-turquoise cwd\">" + $.trim(data) + "&gt;</div>").appendTo(shell.shell);
        shell.input.appendTo(shell.shell);
        shell.input.css("display", "inline-block");
        shell.input.focus();
    },
    
    
    execCmd: function (cmd, callback) {
        "use strict";
        cmd = encodeURIComponent(cmd);
        shell.ajax("exec.php?cmd=" + cmd + "&uid=" + shell.uid, callback);
    },
    
    exec: function (e) {
        "use strict";
        if (e.which === 13) {
            shell.input.css("display", "none");
            var cmd = shell.input.val();
            shell.shell.append("<div class=\"monospace inline\">" + cmd + "</div>");
            
            shell.input.val("");
            
            if (cmd.toLowerCase() === "cls" || cmd.toLowerCase() === "clear") {
                shell.input.appendTo($("#cmdsaver"));
                shell.shell.empty();
                shell.getCwd();
            } else {
                shell.execCmd(cmd, shell.execSuccess);
            }
        }
    },
    execSuccess: function (data) {
        "use strict";
        shell.shell.append("<div class=\"monospace\">" + data.replace("\n", "<br />") + "</div><br />");
        shell.getCwd();
    }
};

$(shell.initShell);