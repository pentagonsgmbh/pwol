/*global alert:false, $:false, window:false */

var allpcs = {
    selectChanged: function () {
        "use strict";
        var select = $("#mode");
        switch (select.val()) {
        case "MAC":
            window.location = "?mode=mac";
            break;
        case "Hostname":
            window.location = "?mode=name";
            break;
        default:
            window.location = "?mode=ip";
            break;
        }
    },
    searchFieldText: function () {
        "use strict";
        
        $(".tile-caption").each(function (i, element) {
            var e = $(this);
            if (e.text().toLowerCase().indexOf($("#search").val().toLowerCase()) > -1) {
                e.parent().css("display", "inline-block");
            } else {
                e.parent().css("display", "none");
            }
        });
    },
    tileClick: function (e) {
        "use strict";
        e = $(e);
        $(".tile").removeClass("tile-big");
        e.addClass("tile-big");
    }
};