/*global $:false, alert:false, notify:false */

var scanner = {
    stop: false,
    
    startIP: [0, 0, 0, 0],
    endIP: [0, 0, 0, 0],
    currentIP: [0, 0, 0, 0],
    pings: 4,
    timeout: 100,
    table: null,
    
    stringArray2intArray: function (a) {
        "use strict";
        var o = [];
        a.forEach(function (v, k) {
            o.push(parseInt(v, 10));
        });
        return o;
    },
    equals: function (a, b) {
        "use strict";
        var i;
        for (i = 0; i < a.length; i += 1) {
            if (a[i] !== b[i]) {
                return false;
            }
        }
        return true;
    },
    ipAdd: function (ip) {
        "use strict";
        var j;
        
        ip[3] += 1;
        for (j = 3; j > 0; j -= 1) {
            if (ip[j] === 255) {
                ip[j - 1] += 1;
                ip[j] = 1;
            }
        }
        return ip;
    },
    ipSubtract: function (ip) {
        "use strict";
        var k;
        
        ip[3] -= 1;
        for (k = 3; k > 0; k -= 1) {
            if (ip[k] === 0) {
                ip[k - 1] -= 1;
                ip[k] = 254;
            }
        }
        return ip;
    },
    
    disableGUI: function () {
        "use strict";
        $("#pings").attr("disabled");
        $("#timeout").attr("disabled");
        $("#start").attr("disabled");
        $("#end").attr("disabled");
        
        $("#btn").val("Stop").addClass("btn-orange").click(scanner.stopPinging);
    },
    stopPinging: function () {
        "use strict";
        scanner.stop = true;
        $("#btn").val("Starten").removeClass("btn-orange").click(scanner.init);
        return false;
    },
    
    init: function () {
        "use strict";
        scanner.stop = false;
        
        scanner.table = $("#result-table");
        
        var s = $("#start").val().match(/\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/), e = $("#end").val().match(/\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/);
        if (s === null || e === null) {
            alert("Eine der beiden IPs ist nicht korrekt");
            return;
        }
        scanner.currentIP = scanner.startIP = scanner.stringArray2intArray(s.toString().split("."));
        scanner.endIP = scanner.ipAdd(scanner.stringArray2intArray(e.toString().split(".")));
        
        scanner.pings = parseInt($("#pings").val(), 10);
        scanner.timeout = parseInt($("#timeout").val(), 10);
        
        scanner.disableGUI();
        
        scanner.sendPing();
        
        return false;
    },
    
    sendPing: function () {
        "use strict";
        alert(scanner.currentIP);
        $.ajax({
            url: "scan.php?ip=" + scanner.currentIP.join(".") + "&count=" + scanner.pings + "&timeout=" + scanner.timeout,
            success: scanner.pingAnswer,
            error: function (xhr, status, error) {alert(status); scanner.nextPing();}
        });
    },
    pingAnswer: function (data) {
        "use strict";
        alert(scanner.currentIP.join(".") + " " + JSON.stringify(data));
        
        if (data === "1") {
            scanner.table.append("<tr><td>" + scanner.currentIP.join(".") + "</td><td>online</td></tr>");
        }
        if (!scanner.stop) {
            scanner.nextPing();
        }
    },
    nextPing: function () {
        "use strict";
        var nextIP = scanner.ipAdd(scanner.currentIP);
        if (!scanner.stop && !scanner.equals(nextIP, scanner.endIP)) {
            scanner.currentIP = nextIP;
            scanner.sendPing();
        } else {
            notify("pWakeOnLan", "Scan abgeschlossen", "../assets/images/icons/ethernet_icon.jpg");
        }
    }
};