/*global $:false, notify:false, setTimeout:false*/

if (typeof String.prototype.startsWith !== 'function') {
    String.prototype.startsWith = function (str) {
        "use strict";
        return this.slice(0, str.length) === str;
    };
}

var nmap = {
    updateInterval: 300,
    resultTable: null,
    scanFile: "",
    
    disableGUI: function () {
        "use strict";
        $("input").attr("disabled", true);
        
        $("#btn").css("display", "none");
        $(".loading-image").css("display", "inline-block");
    },
    enableGUI: function () {
        "use strict";
        $("input").removeAttr("disabled");
        
        $("#btn").css("display", "inline-block");
        $(".loading-image").css("display", "none");
    },
    
    scan: function () {
        "use strict";
        nmap.resultTable = $("#result-table");
        
        nmap.disableGUI();
        
        var cmd = $("#cmd").val();
        $.ajax({
            url: "nmap.php?cmd=" + cmd.replace("/", "%2F"),
            success: nmap.startUpdating,
            error: function (xhr, status, error) {}
        });
    },
    startUpdating: function (data) {
        "use strict";
        nmap.scanFile = data;
        setTimeout(function () { nmap.checkUpdates(); }, nmap.updateInterval);
    },
    checkUpdates: function () {
        "use strict";
        $.ajax({
            url: "scans/" + nmap.scanFile,
            success: nmap.updateCallback,
            error: function (xhr, status, error) { setTimeout(function () { nmap.checkUpdates(); }, nmap.updateInterval); }
        });
    },
    updateCallback: function (data) {
        "use strict";
        var lines = data.split("\n"), i = 0, t = "";
        
        if (lines[lines.length - 1].trim().length === 0) {
            lines.pop();
        }
        
        nmap.resultTable.empty();
        
        for (i; i < lines.length; i += 1) {
            if (!lines[i].startsWith("***")) {
                t = lines[i].split(";");
                nmap.resultTable.append("<tr><td>" + t[0] + "</td><td>" + t[1] + "</td><td>" + t[2] + "</td></tr>");
            }
        }
        
        if (data.indexOf("FINISHED") > -1) {
            nmap.notifyUser();
            nmap.enableGUI();
        } else {
            setTimeout(nmap.checkUpdates, nmap.updateInterval);
        }
    },
    notifyUser: function () {
        "use strict";
        notify("pWakeOnLan", "Nmap-Scan abgeschlossen", "../assets/images/icons/ethernet_icon.jpg");
    }
};