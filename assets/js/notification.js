/*global window:false*/

function notify(title, text, icon) {
    "use strict";
    if (window.webkitNotifications.checkPermission(function () {}) === 0) {
        window.webkitNotifications.createNotification(icon, title, text).show();
    } else {
        window.webkitNotifications.requestPermission();
    }
}