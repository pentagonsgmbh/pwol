<?php

class Image {
	public $width, $height, $img;
	
	public function __construct($width, $height) {
		$this->img = imagecreate($width, $height);
		$this->width = $width;
		$this->height = $height;
		$this->GetColor("#ffffff");
	}
	
    private function hex2rgb($hex) {
		$l = strlen($hex);
		if ($l != 7 and $l != 4 and $l != 6 and $l != 3)
			return imagecolorallocate($this->img, 0, 0, 0);
		
		$hex = str_replace("#", "", $hex);
		if (strlen($hex) == 3)
			$hex = $hex[0] . $hex[0] . $hex[1] . $hex[1] . $hex[2] . $hex[2];
		
		$i = hexdec($hex);
		return array(0xff & ($i >> 0x10), 0xff & ($i >> 0x8), 0xff & $i);
    }
    
	private function GetColor($hex) {
        $c = $this->hex2rgb($hex);
		return imagecolorallocate($this->img, $c[0], $c[1], $c[2]);
	}
	
	private function DrawFilledRectangle($x, $y, $width, $height, $color) {
		$color = $this->GetColor($color);
		imagefilledrectangle($this->img, $x, $y, $x + $width, $y + $height, $color);
	}
    
	public function SetBackgroundcolor($color) {
		$this->DrawFilledRectangle(0, 0, $this->width, $this->height, $color);
	}
    
    public function DrawGradient($x, $y, $height, $width, $color1, $color2) {
        $color1 = $this->hex2rgb($color1);
        $color2 = $this->hex2rgb($color2);
        
        $diff = array();
        foreach ($color1 as $key => $value)
            $diff[$key] = ($value - $color2[$key]) / $width;
        
        for ($i = 0; $i <= $width; $i++) {
            $col = imagecolorallocate($this->img, $color1[0] - $diff[0]*$i, $color1[1] - $diff[1]*$i, $color1[2] - $diff[2]*$i);
            imageline($this->img, $x + $i, $y, $x + $i, $y + $height, $col);
        }
    }
	
	public function DrawText($text, $position, $color, $font_size=12) {
		$color = $this->GetColor($color);
		imagettftext($this->img, $font_size, 0, $position[0], $position[1], $color, "segoeuil.ttf", $text);
	}
	
	public function DrawImage($image) {
		$i = imagecreatefrompng($image);
		imagecopyresized($this->img, $i, 0, 0, 0, 0, $this->width, $this->height, $this->width, $this->height);
		imagedestroy($i);
	}
    
    public function PieDiagram($percent, $color1, $color2, $position, $radius) {
        $this->img = imagerotate($this->img, 270, 0);
        $position = array($position[1], $position[0]);
        
        $color1 = $this->GetColor($color1);
        $color2 = $this->GetColor($color2);
        
        $angle = 360*$percent;
        imagefilledarc($this->img, $position[0], $position[1], $radius * 2, $radius * 2, $angle, 0, $color2, IMG_ARC_PIE);
        
        $h_angle = $angle / 2;
        $offset = 15;
        $offset_x = cos( deg2rad($h_angle) ) * $offset;
        $offset_y = sin( deg2rad($h_angle) ) * $offset;
        imagefilledarc($this->img, $position[0] + $offset_x, $position[1] + $offset_y, $radius * 2, $radius * 2, 0, $angle, $color1, IMG_ARC_PIE);
        $this->img = imagerotate($this->img, 90, 0);
    }
	
	public function Output() {
		header("Content-type: image/png");
		imagepng($this->img);
		imagedestroy($this->img);
	}
}

?>