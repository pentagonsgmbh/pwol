<?php
require("task.php");
require_once("../assets/php/database/database.php");
require("../assets/core/core.php");

function getRepeats() {
    return Database::query("SELECT * FROM task_repeat");
}
function getTypes() {
    return Database::query("SELECT * FROM task_type");
}
function getGroups() {
    return Database::query("SELECT id, name FROM groups");
}

function getTasks() {
    return Database::query("SELECT tasks.*, task_type.name AS T, task_repeat.name AS R, groups.name AS N FROM tasks, task_type, task_repeat, groups WHERE task_type.id = tasks.ref_type AND task_repeat.id = tasks.ref_repeat AND groups.id = tasks.ref_group");
}

class pTask {
    private $name;
    
    public function __construct($name) {
        $this->name = $name;
    }
    
    private function CreateUID() {
        return "pwol-schtasks-" . uniqid();
    }
    
    public function CreateTask($group, $time, $repeat, $type, $month_day=-1) {
        $real_name = $this->name;
        $uid = $this->CreateUID();
        
        Database::exec("INSERT INTO tasks VALUES(NULL, '$real_name', '$uid', $type, $repeat, $month_day, '$time', $group)");
        
        $t = new Task($uid, "\"" . Core::GetCoreScript() . "\" --group-id " . $group . " --action " . $type);
        switch ($repeat) {
            case 1: 
            $t->CreateEveryDay($time);
            break;
            case 2: 
            $t->CreateEveryWorkDay($time);
            break;
            case 3: 
            $t->CreateEveryMonth($time, $month_day);
            break;
        }
    }
    
    public function RemoveTask($uid) {
        $t = new Task($uid);
        $t->Delete();
        Database::exec("DELETE FROM tasks WHERE uid = '$uid'");
    }
    
    private function GetTaskName($name) {
        $r = Database::query("SELECT real_name FROM tasks WHERE uid = '$name'");
        $o = "";
        foreach ($r as $_) {
            $o = $r["real_name"];
        }
        return $o;
    }
    
    public function Delete() {
        $n = $this->GetTaskName($this->name);
        $t = new Task($n);
        $t->Delete();
    }
}

?>