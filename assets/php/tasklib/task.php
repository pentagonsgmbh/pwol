<?php

class Task {
    private $name, $file, $debug;
    
    public function __construct($name, $file="", $debug=0) {
        $this->name = $name;
        $this->file = $file;
        $this->debug = $debug;
    }
    
    public function Delete() {
        $s = "schtasks.exe /DELETE /TN \"" . $this->name . "\" /F";
        if ($this->debug == 1) {
            print $s;
        }
        exec($s);
    }
    
    public function CreateViaParameters($p) {
        $s = "schtasks.exe /CREATE /TN \"" . $this->name . "\" /TR \"" . $this->file . "\" " . $p;
        if ($this->debug == 1) {
            print $s;
        }
        exec($s);
    }
    
    public function CreateEveryDay($time) {
        $this->CreateViaParameters("/SC DAILY /ST $time");
    }
    
    public function CreateEveryWorkDay($time) {
        $this->CreateViaParameters("/SC WEEKLY /D MON,TUE,WED,THU,FRI /ST $time");
    }
    
    public function CreateEveryMonth($time, $day) {
        $this->CreateViaParameters("/SC MONTHLY /D $day /ST $time");
    }
    
    public function ChangeViaParameters($p) {
        $s = "schtasks.exe /CHANGE /TN \"" . $this->name . "\" " . $p;
        if ($this->debug) {
            print $s;
        }
        exec($s);
    }
    
    public function ChangeFile($file) {
        $this->ChangeViaParameters("/TR \"$file\"");
    }
    
    public function ChangeTime($time) {
        $this->ChangeViaParameters("/ST \"$time\"");
    }
}

?>