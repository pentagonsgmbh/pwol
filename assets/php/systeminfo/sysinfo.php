<?php
set_time_limit(0);

class systeminfo {
    private $i;
    
    public function __construct() {
        exec("systeminfo", $this->i);
    }
    
    private function GetKeyValue($i) {
        $split = preg_split("/\\:\s{1,100}/", $this->i[$i]);
        $o = array("key"=>"");
        foreach (array_slice($split, 0, count($split) - 1) as $e) {
            $o["key"] .= $e . ": ";
        }
        $o["key"] = substr($o["key"], 0, strlen($o["key"]) - 2);
        $o["value"] = array_pop($split);
        
        return $o;
    }
    
    public function GetHostname() {
        $kv = $this->GetKeyValue(1);
        return $kv["value"];
    }
    
    public function GetOS() {
        $name = $this->GetKeyValue(2);
        $version = $this->GetKeyValue(3);
        $installed = $this->GetKeyValue(10);
        return array("name"=>$name["value"], "version"=>$version["value"], "install"=>$installed["value"]);
    }
    
    public function GetBIOS() {
        $b = $this->GetKeyValue(17);
        return $b["value"];
    }
    
    private function MB2INT($mb) {
        return str_replace(array(".", " MB"), array("", ""), $mb) + 0;
    }
    
    public function GetRAM() {
        $all = $this->GetKeyValue(24);
        $available = $this->GetKeyValue(25);
        $all = $this->MB2INT($all["value"]);
        $a = $this->MB2INT($available["value"]);
        return array("all"=>$all, "available"=>$a);
    }
}

class CPU {
    public function GetUsage() {
        $r = explode("\n", trim(shell_exec("wmic cpu get loadpercentage")));
        return $r[1];
    }
    
    public function GetCores() {
        $r = explode("\n", trim(shell_exec("wmic cpu get NumberOfCores")));
        return $r[1];
    }
}


class NetworkAdapters {
    public $adapters = array();
    
    public function __construct() {
        $this->FindAdapters();
    }
    
    private function FindAdapters() {
        $adapters = array();
        
        exec("netsh interface ipv4 show interface", $table);
        array_pop($table);
        
        $table = array_slice($table, 3);
        
        foreach ($table as $row) {
            $split = preg_split("/\s\s/", $row);
            $name = array_pop($split);
            $middle = array_pop($split);
            if (trim($middle) == "") {
                $state = array_pop($split);
            }
            else {
                $state = $middle;
            }
            
            $adapters[trim($name)] = array("state"=>$state, "ip"=>"", "subnetmask"=>"", "dhcp"=>"");
        }
        
        foreach ($adapters as $name => $config) {
            /*$c = "netsh interface ip show config name=\"$name\" |find \"%s\"";
            $ip = shell_exec(sprintf($c, "IP"));
            print $name . "\n" . $ip . "\n";
            $dhcp = shell_exec(sprintf($c, "DHCP"));
            print $dhcp . "\n";
            $subnetmask = shell_exec(sprintf($c, "ask"));
            print $subnetmask . "\n";
            */
            exec("netsh interface ip show config name=\"$name\"", $c);
            $ip = $dhcp = $subnetmask = null;
            
            foreach ($c as $_) {
                if (strpos(" " . $_, "IP") > 0) {
                    preg_match("/\\d{1,3}\.\\d{1,3}\.\\d{1,3}\.\\d{1,3}/", $_, $ip);
                    $ip = $ip[0];
                    
                } else if (strpos(" " . $_, "DHCP") > 0) {
                    $dhcp = preg_split("/\\:/", $_);
                    $dhcp = trim($dhcp[1]);
                } else if (strpos(" " . $_, "ask") > 0) {
                    preg_match_all("/\\d{1,3}\.\\d{1,3}\.\\d{1,3}\.\\d{1,3}/", $_, $subnetmask);
                    $subnetmask = array_pop($subnetmask[0]);
                }
            }
            $this->adapters[$name] = array("state"=>$config["state"], "dhcp"=>$dhcp, "ip"=>$ip, "subnetmask"=>$subnetmask);
        }
    }
}

?>