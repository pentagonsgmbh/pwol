<?php
require_once("database.php");

function addGroup($name) {
    $name = json_encode($name);
    Database::exec("INSERT INTO groups VALUES(NULL, $name)");
}
function rmGroup($id) {
    Database::exec("DELETE FROM groups WHERE id = $id; DELETE FROM pglink WHERE ref_group = $id");
	
	require("../assets/php/tasklib/task.php");
	$task_delete = "DELETE FROM tasks WHERE uid = '%s'; ";
	$tasks = "";
	foreach (Database::query("SELECT uid FROM tasks WHERE ref_group = $id") as $task) {
		$t = new Task($task["uid"]);
		$t->Delete();
		$tasks .= sprintf($task_delete, $task["uid"]);
	}
	Database::exec($tasks);
}

function getGroups() {
    return Database::query("SELECT * FROM groups ORDER BY name");
}
function getGroupName($id) {
    $res = Database::query("SELECT name FROM groups WHERE id = $id");
    $o = 0;
    foreach ($res as $r) {
        $o = $r["name"];
    }
    return $o;
}

function getPCsInGroup($id) {
    return Database::query("SELECT pcs.* FROM pcs WHERE pcs.id IN (SELECT pglink.ref_pc FROM pglink WHERE pglink.ref_group = $id) ORDER BY ip");
}
function getPCsNotInGroup($id) {
    $o = array();
    $c = Database::query("SELECT COUNT(pcs.id) AS C FROM pcs WHERE pcs.id NOT IN (SELECT pglink.ref_pc FROM pglink WHERE pglink.ref_group = $id)");
    foreach ($c as $_) {
        $o["count"] = $_["C"];
    }
    $o["result"] = Database::query("SELECT pcs.* FROM pcs WHERE pcs.id NOT IN (SELECT pglink.ref_pc FROM pglink WHERE pglink.ref_group = $id)");
    return $o;
}

function addPCsToGroup($group_id, $pcs) {
    $query = "INSERT INTO pglink VALUES(NULL, %s, $group_id);";
    $q = "";
    foreach (explode(";", $pcs) as $id) {
        $q .= sprintf($query, $id);
    }
    Database::exec($q);
}
function rmPCFromGroup($group_id, $pc) {
    Database::exec("DELETE FROM pglink WHERE ref_pc = $pc AND ref_group = $group_id");
}

?>