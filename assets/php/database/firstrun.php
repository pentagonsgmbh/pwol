<?php
require("database.php");

if (file_exists("database.sqlite")) {
	unlink("database.sqlite");
}

Database::exec("CREATE TABLE pcs (id INTEGER PRIMARY KEY, ip TEXT, mac TEXT, name TEXT)");
Database::exec("CREATE TABLE groups (id INTEGER PRIMARY KEY, name TEXT)");
Database::exec("CREATE TABLE pglink (id INTEGER PRIMARY KEY, ref_pc TEXT, ref_group TEXT)");

Database::exec("CREATE TABLE tasks (id INTEGER PRIMARY KEY, real_name TEXT, uid TEXT, ref_type INTEGER, ref_repeat INTEGER, month_day INTEGER, time TEXT, ref_group INTEGER)");
Database::exec("CREATE TABLE task_type (id INTEGER PRIMARY KEY, name TEXT)");
Database::exec("CREATE TABLE task_repeat (id INTEGER PRIMARY KEY, name TEXT)");

Database::exec("INSERT INTO task_type VALUES(NULL, 'Starten');");
Database::exec("INSERT INTO task_type VALUES(NULL, 'Stoppen');");
Database::exec("INSERT INTO task_type VALUES(NULL, 'Neustarten');");

Database::exec("INSERT INTO task_repeat VALUES(NULL, 'T&auml;glich');");
Database::exec("INSERT INTO task_repeat VALUES(NULL, 'Werktags');");
#Database::exec("INSERT INTO task_repeat VALUES(NULL, 'Monatlich');"); //lazy

?>