/*global $:false, setTimeout:false*/

$.jqtile = {};

/* live-tile.js */
$.jqtile.tile = {
    liveQueue: [],
    queueLiveTiles: function () {
        "use strict";
        $(".tile[data-live-url]").each(function (i, element) {
            var e = $(this);
            $.jqtile.tile.liveQueue.push(e);
        });
        
        $.jqtile.tile.startUpdating();
    },
    currentUpdateTile: 0,
    startUpdating: function () {
        "use strict";
        if ($.jqtile.tile.liveQueue.length > 0) {
            $.jqtile.tile.updateTile(0);
        }
    },
    updateTile: function () {
        "use strict";
        if ($.jqtile.tile.currentUpdateTile === $.jqtile.tile.liveQueue.length) {
            $.jqtile.tile.currentUpdateTile = 0;
            setTimeout(function () {
                $.jqtile.tile.updateTile(0);
            }, 10000);
            return;
        }
        
        
        $.ajax({
            dataType: "json",
            url: $.jqtile.tile.liveQueue[$.jqtile.tile.currentUpdateTile].attr("data-live-url"),
            success: $.jqtile.tile.ajaxSuccess,
            error: function (xhr, status, error) {
                $.jqtile.tile.currentUpdateTile += 1;
                $.jqtile.tile.updateTile();
            }
        });
    },
    ajaxSuccess: function (json) {
        "use strict";
        var e = $.jqtile.tile.liveQueue[$.jqtile.tile.currentUpdateTile], img = $("<img class=\"live-image\" src=\"" + json["background-image"] + "\" />");
        $("img.live-image[old-image]").filter(function () {
            var k = $(this);
            if (k.css("top") === "150px") {
                k.remove();
            }
        });
        e.find("img.live-image").attr("old-image", 1);
        
        img.load(function () {
            $(this).css("top", "0px");
            $(this).parent().find("img.live-image[old-image]").css("top", "150px");
        });
        e.append(img);
        e.find(".tile-caption").text(json["tile-text"]);
        
        $.jqtile.tile.currentUpdateTile += 1;
        $.jqtile.tile.updateTile();
    }
};


/* drag-n-drop.js */
$.jqtile.dnd = {
    initialPosition: [0, 0],
    dndStarted: 0,
    elementClone: null,
    initDrag: function (e) {
        "use strict";
        $.jqtile.dnd.initialPosition = [e.pageX, e.pageY];
        $.jqtile.dnd.dndStarted = 1;
        
        $.jqtile.dnd.elementClone = $(this).clone();
        $(this).css("visibility", "hidden");
    },
    drag: function (e) {
        "use strict";
        var c = $.jqtile.dnd.elementClone;
        
        
    }
};


/* tile-opening.js */
$.jqtile.tileOpening = {
    loadTiles: function () {
        "use strict";
        $("a.tile[href]").click(function () {
            var e = $(this); //todo
            
            setTimeout("window.location = '" + e.attr("href") + "';", 1);
            return false;
        });
    },
    setTransformOrigin: function (element, origin) {
        "use strict";
        element.css({
            "-webkit-transform-origin": origin,
            "-moz-transform-origin": origin,
            "-ms-transform-origin": origin,
            "-o-transform-origin": origin,
            "transform-origin": origin
        });
    }
};

/* checktile.js */
$.jqtile.checktile = {
    bind: function () {
        "use strict";
        $(".tile[checkable]").click($.jqtile.checktile.toggleCheck);
    },
    toggleCheck: function () {
        "use strict";
        var e = $(this);
        if (e.attr("checked") === "checked") {
            e.removeAttr("checked");
        } else {
            e.attr("checked", "checked");
        }
    }
};


/* start handler */
$(function () {
    "use strict";
    $.jqtile.checktile.bind();
    $.jqtile.tile.queueLiveTiles();
    $.jqtile.tileOpening.loadTiles();
});