<?php
require("../assets/php/tasklib/ptask.php");
?>
<!doctype html>
<html>
    <head>
        <link rel="stylesheet" href="../assets/tileosaur/css/tileosaur.min.css" />
        <link rel="stylesheet" href="../assets/images/backgrounds.css" />
        <link rel="stylesheet" href="../assets/css/custom.css" />
        <link rel="icon" href="../assets/images/icon.ico" />
        <title>Tasks &middot; pWOL</title>
    </head>
    <body class="body body-tasks">
        <h1 class="page-header">
            <a href="../" class="back-icon"></a>
            Tasks
        </h1>
        
        <div class="app-container">
            <div class="container">
                <h2 class="sub-header text-purple">Task erstellen</h2>
                <form action="addtask.php" method="get">
                    <input type="text" class="input-purple add-pc" name="name" placeholder="Name" autofocus /><br />
                    <input type="text" class="input-purple add-pc" name="time" placeholder="Zeit" /><br />
                    
                    <select name="repeat" class="select input-purple select-wide">
                    <?php
                    
                    foreach (getRepeats() as $repeat) {
                        $id = $repeat["id"]; $name = $repeat["name"];
                        echo "
                        <option value=\"$id\">$name</option>";
                    }
                            
                    ?>
                    </select><br />
                    <select name="type" class="select input-purple select-wide">
                    <?php
                    
                    foreach (getTypes() as $type) {
                        $id = $type["id"]; $name = $type["name"];
                        echo "
                        <option value=\"$id\">$name</option>";
                    }
                            
                    ?>
                    </select><br />
                    <select name="group" class="select input-purple select-wide">
                    <?php
                    
                    foreach (getGroups() as $group) {
                        $id = $group["id"]; $name = $group["name"];
                        echo "
                        <option value=\"$id\">$name</option>";
                    }
                            
                    ?>
                    </select><br />
                    
                    <input type="submit" class="btn btn-purple btn-add-pc" value="Erstellen" />
                </form>
            </div>
            <?php
            $task_text = "
            <div class=\"container\">
                <h2 class=\"sub-header text-purple\">
                    %s
                    <a href=\"rmtask.php?id=%s\" class=\"back-icon trash-icon\"></a>
                </h2>
                <table class=\"table table-zebra\">
                    <tbody>
                        <tr>
                            <td>Zeit:</td>
                            <td>%s</td>
                        </tr>
                        <tr>
                            <td>Wiederholung:</td>
                            <td>%s</td>
                        </tr>
                        <tr>
                            <td>Modus:</td>
                            <td>%s</td>
                        </tr>
                        <tr>
                            <td>Gruppe:</td>
                            <td>%s</td>
                        </tr>
                    </tbody>
                </table>
            </div>";
            
            foreach (getTasks() as $task) {
                printf($task_text, $task["real_name"], $task["uid"], $task["time"], $task["R"], $task["T"], $task["N"]);
            }

            ?>
        </div>
    </body>
</html>