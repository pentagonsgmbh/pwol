<?php
require("../assets/php/tasklib/ptask.php");

if (isset($_GET["name"]) and isset($_GET["time"]) and isset($_GET["repeat"]) and isset($_GET["type"]) and isset($_GET["group"])) {
    $name = $_GET["name"]; $time = $_GET["time"]; $repeat = $_GET["repeat"]; $type = $_GET["type"]; $group = $_GET["group"]; 
    $t = new pTask($name);
    $t->CreateTask($group, $time, $repeat, $type, 1);
}
header("Location: index.php");

?>