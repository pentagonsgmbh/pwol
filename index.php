<!doctype html>
<html>
    <head>
        <link rel="stylesheet" href="assets/tileosaur/css/tileosaur.min.css" />
        <link rel="stylesheet" href="assets/images/backgrounds.css" />
        <link rel="stylesheet" href="assets/css/custom.css" />
        <link rel="icon" href="assets/images/icon.ico" />
        <title>pWOL</title>
    </head>
    <body class="body tree-photo-2">
        <div class="vertical-center-container">
            <div class="vertical-center">
                <h1 class="tile-header">pWakeOnLan</h1>
                <div class="tile-container">
                    <div class="tile-group">
                        <a href="groups/" class="tile tile-red tile-big">
                            <div class="tile-caption">
                                PC-Gruppen
                            </div>
                            <img class="tile-logo" src="assets/images/icons/groups.png" />
                        </a>
                        <a href="server/" class="tile tile-orange" data-live-url="server/live.php">
                            <div class="tile-caption">
                                Server-Status
                            </div>
                        </a>
                        
                        <br />
                        
                        <a href="tasks/" class="tile tile-purple">
                            <div class="tile-caption">
                                Tasks
                            </div>
                            <img class="tile-logo" src="assets/images/icons/tasks.png" />
                        </a>
                        <a href="allpcs/" class="tile tile-green tile-big">
                            <div class="tile-caption">
                                Alle PCs
                            </div>
                            <img class="tile-logo" src="assets/images/icons/allpcs.png" />
                        </a>
                    </div>
                    <div class="tile-group">
                        <a href="shell/" class="tile tile-turquoise">
                            <div class="tile-caption">
                                Server Shell
                            </div>
                            <img class="tile-logo" src="assets/images/icons/shell.png" /> 
                        </a>
                        
                        <a href="scanner/" class="tile tile-blue">
                            <div class="tile-caption">
                                Netzwerkscanner
                            </div>
                            <img class="tile-logo" src="assets/images/icons/ethernet.png" />
                        </a>
                        <br />
                    </div>
                </div>
            </div>
        </div>
        
        <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
        <script type="text/javascript" src="assets/tileosaur/js/tileosaur.min.js"></script>
    </body>
</html>